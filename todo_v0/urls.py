from django.conf.urls import url

from django.views.decorators.csrf import csrf_exempt

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^mark_done/(?P<id>\d+)/$', views.mark_done, name="mark_done"),
    url(r'^mark_todo/(?P<id>\d+)/$', views.mark_todo, name="mark_todo"),
    url(r'^new_todo/$', views.new_todo, name="new_todo")
]