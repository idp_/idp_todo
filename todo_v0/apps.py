# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.apps import AppConfig


class TodoV0Config(AppConfig):
    name = 'todo_v0'
