# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.shortcuts import render, redirect
from helpers import random_color

# import models
from .models import TodoItem

#import forms
from .forms import NewTodoForm



# Create your views here.


def index(request):

    todo_list = TodoItem.objects.filter(done=False)
    done_list = TodoItem.objects.filter(done=True)

    form = NewTodoForm()

    return render(request, 'index.html', locals())

def mark_done(request, id):

    done_item = TodoItem.objects.get(id=id)
    done_item.done = True
    done_item.save()

    return redirect('/')

def mark_todo(request, id):
    done_item = TodoItem.objects.get(id=id)
    done_item.done = False
    done_item.save()

    return redirect('/')

def new_todo(request):
    if request.method == 'POST':
        form = NewTodoForm(request.POST)
        if form.is_valid():
            todo_txt = form.cleaned_data['new_item_txt']

            new_item = TodoItem()
            new_item.txt = todo_txt
            new_item.color = random_color()

            new_item.save()

    return redirect('/')
