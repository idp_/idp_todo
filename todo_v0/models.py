# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from django.utils.encoding import python_2_unicode_compatible

from django.db import models

# Create your models here.
@python_2_unicode_compatible
class TodoItem(models.Model): 
    id = models.AutoField(primary_key=True)
    txt = models.CharField(max_length=1000)
    color = models.CharField(max_length=12)
    done = models.BooleanField(default=False)

    def __str__(self):
        return "[%s] %s" % (self.id, self.txt)