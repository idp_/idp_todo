from django import forms

class NewTodoForm(forms.Form):
    new_item_txt = forms.CharField(label='', max_length=1000, widget=forms.TextInput(attrs={'placeholder': 'todo'}) )
    # new_item_color = forms.CharField(label='', max_length=12, required=False, widget=forms.TextInput(attrs={'placeholder': 'color'}) )